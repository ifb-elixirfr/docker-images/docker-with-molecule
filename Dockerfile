FROM docker:latest

ARG MOLECULE_VERSION=4.0.0
ARG ANSIBLE_VERSION=5.4.0

RUN apk update

RUN apk add git curl build-base \
    python3 python3-dev \
    py3-pip py3-setuptools py3-cryptography \
    openssl openssl-dev \
    libffi libffi-dev \
    rsync


RUN pip install --break-system-packages ansible==${ANSIBLE_VERSION} ansible-lint molecule==${MOLECULE_VERSION} molecule-plugins[docker] yamllint
